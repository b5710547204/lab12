package downloader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.swing.SwingWorker;

// This is a decorator (wrapper) for an InputStream that you can monitor read progress.
//import javax.swing.ProgressMonitorInputStream;

/** 
 *  Download a URL to a File.
 *
 * @author James Brucker,Patinya Yongyai
 */
public class URLReader extends SwingWorker<Void, Integer>{
	private DownloaderUI ui;
	private URL url;
	private URLConnection connection;
	private InputStream instream;
	private int bytesRead; // number of bytes read so far
	private static final int BUFFERSIZE = 32*1024; // default buffer size for read
	/** length of URL stream, lazily determined. */
	private int urlsize;
	/** The output file. */
	private File outfile;
	/** output file to write to */
	private FileOutputStream outstream;
	
	/** 
	 * Initialize a new URL Reader.
	 * @param url is the URL to read from
	 * @param outfile is a File output to write output to. If it is a writable directory
	 *        then a file is created in that directory with same name as the download resource.
	 * @throws IOException if URLConnection fails, 
	 * @throws FileNotFoundException if outfile not found or cannot be written to
	 * @precondition url is a valid URL, outfile is a valid writable file or directory. 
	 */
	
	public URLReader ( URL url, File outfile , DownloaderUI ui) throws IOException{
		this(url,outfile);
		this.ui = ui;
	}
	
	public URLReader( URL url, File outfile) throws IOException {
		
		if (url == null) throw new IllegalArgumentException("url cannot be null");
		this.url = url;
		this.bytesRead = 0;
		if (outfile == null) throw new IllegalArgumentException("output file cannot be null");
		if (! outfile.canWrite()) 
			throw new IllegalArgumentException("output file "+outfile.getName()+" is not writable");
		if ( outfile.isDirectory() ) {
			// create a file in directory having same name as the URL resource name
			String filename = url.getPath();
			int k = filename.lastIndexOf("/");
			if (k >= 0) {
				if (k == filename.length()-1) filename = "noname";
				else filename = filename.substring(k+1); // could fail
			}
			outfile = new File(outfile, filename); 
		}
		this.outfile = outfile;
		// don't open connection yet -- the server might close it before we run()
		// create output writer
		outstream = new FileOutputStream( outfile ); // "rwd" mode?
	}
	
	private InputStream getInputStream() throws IOException {
		connection = url.openConnection();
		return connection.getInputStream();
	}
	
	/**
	 * Get the number of bytes downloaded so far.
	 * @return number of bytes downloaded from URL so far.
	 */
	public int getBytesRead( ) { return bytesRead; }
	
	/** 
	 * Read the URL connection and save bytes to output file. 
	 * This method will block until the entire URL content is read.
	 * While reading, the bytesRead object attribute is regularly updated.
	 * @return the number of bytes actually read
	 */
	public int readURL( ) {
		int percentage = 0;
		bytesRead = 0;
		int buffsize = BUFFERSIZE;
		byte [] buff = new byte[buffsize];
		try {
			instream = getInputStream();
			do {
				int n = instream.read( buff );
				if ( n < 0 ) break; // read returns -1 at end of input
				outstream.write(buff, 0, n);
				bytesRead += n;
				
				publish((int)Math.round((bytesRead*1.0/getSize()) * 100));
			} while ( true );
		} catch (IOException e) { 
			System.err.println("readURL: "+e); 
		} finally {
			if ( instream != null ) try { 
				instream.close(); 
			} catch (IOException e) { /* who cares? its not my data. */ }
			try {
				outstream.close();
			} catch (IOException e) { /* ignore it */ }
		}
		return bytesRead;
	}
	
	/** 
	 * Get the size in bytes of the URL to download.
	 * @return the size in bytes of the URL to download, -1 if cannot determine.
	 */
	public int getSize( ) {
		if ( urlsize > 0 ) return urlsize;
		if ( url == null ) return 0;
		try {
			URLConnection connection = url.openConnection();
			urlsize = connection.getContentLength();
		} catch (java.io.IOException e) {
			urlsize = -1;
		}
		return urlsize;
	}
	
	/**
	 * Get actual name of the output file.
	 * @return string name with path of the output file
	 */
	public String getOutputFile() {
		return outfile.getPath();
	}
	
	/** test the code 
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
		String URLSTRING = "http://www.ku.ac.th/web2012/resources/template2/ui/images/logo.png";
		URL url = new URL(URLSTRING);
		// this works on Linux that has a writable "/tmp" directory
		
		try {
			URLReader reader = new URLReader(url, new File("/tmp") );
			int bytes = reader.readURL();
			System.out.printf("Read %d bytes\n", bytes);
		} catch (IOException e) {
			System.out.println( e );
		}	
	}

	@Override
	protected Void doInBackground() throws Exception {
		this.ui.addMessage("Downloading...\n");
		readURL();
		return null;
	}
	
	protected void process(List<Integer> published){
		for(int p: published){
			this.ui.progressBar.setValue(p);
		}
	}
	
	protected void done(){
		int bytes = getBytesRead();
		this.ui.addMessage("Downloaded "+bytes+" bytes");
	}
}
